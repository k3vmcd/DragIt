// define variable now and load default values when DOM loads
var defaults = {};

function fadeOut(elem, time, stepSize) {
    stepSize = stepSize || 0.01;
    if (elem.style.opacity === '') elem.style.opacity = '1';
    (function f() {
        if (elem.style.opacity <= 0) {
            elem.style.display = 'none';
            elem.style.opacity = '';
            return;
        }
        elem.style.opacity -= stepSize;
        setTimeout(f, time * stepSize);
    })();
}

// prevent the user from adding an engine url that would break the extension's functionality
// also, update any previously saved engines from previous versions of this extension
function clean(engine) {
	return engine.replace(/^https?:\/\//,'').replace('%s','');
}

function updateDOM(opts) {
    document.getElementById('search-engine').value = opts.searchEngine;
    document.getElementById('ignore-input').checked = opts.ignoreInput;
    document.getElementById('use-ssl').checked = opts.useSSL;
    document.getElementById('protocol').innerText = opts.useSSL ? 'https://' : 'http://';
}

function saveSearchEngine() {
    var field = document.getElementById('search-engine');
    var engine = clean(field.value);
    chrome.storage.sync.set({
        // update searchEngine with user entry, but clean the engine first
        searchEngine: engine
    }, function() {
        // confirm user entry saved
        var status = document.getElementById('status');
        status.style.display = '';
        setTimeout(function() {
            fadeOut(status, 750, 0.1);
        }, 1500);
        
        field.value = engine;
    });
}

function saveOptions() {
    var opt = {};
    if (this.id === 'use-ssl') {
        opt.useSSL = this.checked;
        document.getElementById('protocol').innerText = this.checked ? 'https://' : 'http://';
    } else if (this.id === 'ignore-input') {
        opt.ignoreInput = this.checked;
    }
    chrome.storage.sync.set(opt);
}

function resetOptions() {
    updateDOM(defaults);
    chrome.storage.sync.set(defaults);
}

// Restores select box state to saved value from synced storage.
function restoreOptions() {
    // first argument gives default values if no values are saved
    chrome.storage.sync.get(defaults, updateDOM);
}

document.addEventListener('DOMContentLoaded', function() {
    chrome.storage.sync.get(function(items) {
        defaults = items.defaults; // load defaults set in background.js
        restoreOptions();
    });

    document.getElementById('save-search-engine').addEventListener('click', saveSearchEngine);
    document.getElementById('ignore-input').addEventListener('change', saveOptions);
    document.getElementById('use-ssl').addEventListener('change', saveOptions);
    document.getElementById('reset-options').addEventListener('click', resetOptions);
});

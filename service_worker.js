var defaults = {
	searchEngine: 'www.google.com/search?q=',
	ignoreInput: true,
	useSSL: true
};

function clean(url) {
	url = url.replace(/^https?:\/\//, '').replace('%s', '');
	chrome.storage.sync.set({ searchEngine: url });
	return url;
}

chrome.storage.sync.set({ defaults: defaults }, function() { // set default values for the rest of extension
	chrome.storage.sync.get(function init(items) {

		var engine = items.searchEngine || defaults.searchEngine;
		var ssl = items.useSSL || defaults.useSSL;

		// Old versions use an invalid URL, so make sure the search engine
		// URL is 'clean'
		if (engine.match(/^https?:\/\//)) {
			engine = clean(engine);
		}

	    //Listen and set new search engine on user's save from options page
	    chrome.storage.onChanged.addListener(function(changes) {
	        if (changes.hasOwnProperty('searchEngine')) {
	            engine = changes.searchEngine.newValue;
	        } else if (changes.hasOwnProperty('useSSL')) {
				ssl = changes.useSSL.newValue;
			}
	    });

	    // listen for link and text drag events
	    chrome.runtime.onConnect.addListener(function(port) {
	        port.onMessage.addListener(function(data) {
	            if (data.message === 'tab') {
	                chrome.tabs.query({
						active: true, 
						lastFocusedWindow: true
					}, function(tabs) {
						var fg = (data.x_dir === 1);
						var protocol = ssl ? 'https://' : 'http://';
	                    var link = data.url || (protocol + engine + data.text);

						// launch new tab
	                    chrome.tabs.create({'url' : link, 'active' : fg});
	                });
	            }
	        });
	    });
	});
});

var ignoreInput;

function cancel(e) {
    if (!targetIsInput(e)) {
        if (e.preventDefault) e.preventDefault();
        return false;
    }
}

//Determine drag direction
function dragStart(e) {
	start_x = e.screenX;
}

function targetIsInput(e) {
    if (ignoreInput === false) return false;

    var nodeName = String(e.target.nodeName).toLowerCase();
    var type = e.target.type;
    if (nodeName === 'textarea' ||
        (nodeName === 'input' &&
         (type === 'text' || type === 'number'))) {
        return true;
    }
    return false;
}

function onDrop(e) {
    if (targetIsInput(e)) return;

    if (e.preventDefault) e.preventDefault();

    var url = e.dataTransfer.getData('url');
    var text = e.dataTransfer.getData('text');
    var x_dir = (e.screenX < start_x) ? -1 : 1;

    if (url || text) {
        chrome.runtime.connect().postMessage({
            message: 'tab',
            url: url,
            text: text,
            x_dir: x_dir
        });
        return false;
    }
}

chrome.storage.sync.get(function init(items) {
    ignoreInput = items.ignoreInput || items.defaults.ignoreInput;

    chrome.storage.onChanged.addListener(function(changes) {
        if (changes.hasOwnProperty('ignoreInput')) {
            ignoreInput = changes.ignoreInput.newValue;
        }
    });

    document.addEventListener('dragstart', dragStart);
    document.addEventListener('dragover', cancel);
    document.addEventListener('drop', onDrop);
});
